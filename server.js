

const connection = require('./mysql');
const img = require('./icon');
const express = require('express');
const app = express();
const request = require('request');
const API_KEY = 'APPID=4db52e608ec76abd8e83cd277f67e434';
const URL = 'https://api.openweathermap.org/data/2.5';


let date = new Date;  


//-----------------------------------------------------------------------------------------------------------------
// #URL Main URL with immage
app.get('/', function (req, res) {
  res.send(" Welcome to free weather forecast: " );
});

//------------------------------------------------------------------------------------------------------------------
// #URL Information about weather by coordinate 
app.get('/coord', function (req, res) {
  let { lat , long } = req.query;
  
  // Set default coordinate if enter nothing
  if (lat === '' && long === ''){
    lat = 55.7558;
    long = 37.6173;
  }
  
  // Request to api with 2 paramet: "lat" and "long" of coordinate 
  request(`${URL}/weather?lat=${lat}&lon=${long}&${API_KEY}`, { json: true }, (err, response, body) => {
    if (err) {
      console.log("Error: ", err);
    }
        
    // Insert into DataBase new information about responce 
    let date = new Date;
    const values = [body.coord.lat, body.coord.lon, body.name, date];
    let sql_insert = `INSERT INTO coordinate_info (latitude, longitude, city, date) VALUES (?, ?, ?, ?)`;
    connection.query(sql_insert,values, function (err){
      console.log("Error: ", err);
    });
    
    // console.log(values);
    res.send(body);
    
  });

});


//-----------------------------------------------------------------------------------------------------------
// #URL Info from DataBase 
app.get('/info', function (req, res) {


  const values = [body.coord.lat, body.coord.lon, body.name, date];
  // showing information from table coordinate_info in database
  let sql_insert = `SELECT (latitude, longitude, city, date) FROM coordinate_info VALUES (?, ?, ?, ?)`;
    connection.query(sql_insert,values, function (err){
      console.log("Error: ", err);
    });

  console.log(res);
});






//-----------------------------------------------------------------------------------------------------------
// Starting API 
app.listen(3012, function () {
  console.log('API app started ... ');
});



